echo "net.core.default_qdisc=fq" >> /etc/sysctl.conf

echo "net.ipv4.tcp_congestion_control=bbr" >> /etc/sysctl.conf

sysctl -p

lsmod | grep bbr

apt-get install python-pip

pip install git+https://github.com/shadowsocks/shadowsocks.git@master

cat ./conf.json > /etc/ss-conf.json

sudo ssserver -c /etc/ss-conf.json -d start